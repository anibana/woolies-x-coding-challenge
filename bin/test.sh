#!/usr/bin/env bash

for opt in "$@"; do
  case ${opt} in
    --integration)
      INTEGRATION=true
      ;;
    --coverage)
      COVERAGE=true
      ;;
  esac
done

echo "**********OPTIONS***********"
echo "integration:         ${INTEGRATION:-false}"
echo "coverage:            ${COVERAGE:-false}"
echo "****************************"

export PATH="$PATH:./node_modules/.bin"

echo "**********TEST ENV***********"
cat test.env
echo "****************************"

export $(cat test.env | xargs)

if [ -n "$INTEGRATION" ]; then
  jest ./test
elif [ -n "$COVERAGE" ]; then
  jest --coverage
else
  jest
fi
