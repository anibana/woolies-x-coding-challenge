const nodeExternals = require('webpack-node-externals')
const path = require('path')
const webpack = require('webpack')

const entry = [
  './src/index.js'
]

const plugins = [
  new webpack.NamedModulesPlugin(),
  new webpack.NoEmitOnErrorsPlugin()
]

const output = {
  path: path.resolve(__dirname, '../dist'),
  filename: 'server.js'
}

const target = 'node'

const externals = [nodeExternals()]

const buildModule = {
  rules: [
    {
      test: /.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      options: {
        presets: [['env', {targets: {node: 'current'}}], 'stage-0'],
        plugins: [
          ['transform-object-rest-spread', {useBuiltIns: true}]
        ]
      }
    },
  ]
}

const resolve = {
  alias: {
    '@core': path.resolve(__dirname, '../src/core'),
    '@config': path.resolve(__dirname, '../src/config'),
    '@shared': path.resolve(__dirname, '../src/v1/shared')
  }
}

module.exports = {
  resolve,
  entry,
  externals,
  plugins,
  output,
  target,
  module: buildModule
}
