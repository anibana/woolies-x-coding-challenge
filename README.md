# Woolies Coding Challenge

## Deployed URL
https://powerful-retreat-80119.herokuapp.com/api/answers

## Libraries used
* Express
* Fluture
* Yup

## Node Version
v12.x

## Installation
Install dependencies `npm i`

## How to run test
`npm run test`
With test coverage - `npm run test:coverage`


## Running on local
1. Create a .env file to set environment variables. Use dev.template and change the values.
`cp dev.template .env`
2. Run the application
`npm run dev`
This will run the application on port 3000


## Building and running the artifact
`npm run build && npm run start`

# Exercise 1 - get user and token
[Get User](https://powerful-retreat-80119.herokuapp.com/api/answers/user)

## Result
```
[
  {
    "passed": true,
    "url": "https://powerful-retreat-80119.herokuapp.com/api/answers/user",
    "message": "Name returned correctly: Aljon Aniban."
  }
]
```

# Exercise 2 - sort products
[LOW](https://powerful-retreat-80119.herokuapp.com/api/answers/sort?sortOption=Low)
[HIGH](https://powerful-retreat-80119.herokuapp.com/api/answers/sort?sortOption=High)
[ASCENDING](https://powerful-retreat-80119.herokuapp.com/api/answers/sort?sortOption=Ascending)
[DESCENDING](https://powerful-retreat-80119.herokuapp.com/api/answers/sort?sortOption=Descending)
[RECOMMENDED](https://powerful-retreat-80119.herokuapp.com/api/answers/sort?sortOption=Recommended)

## Result
```
[
  {
    "passed": true,
    "url": "https://wooliesx.herokuapp.com/api/answers/sort",
    "message": "Ascending Sort Passed"
  },
  {
    "passed": true,
    "url": "https://wooliesx.herokuapp.com/api/answers/sort",
    "message": "Descending Sort Passed"
  },
  {
    "passed": true,
    "url": "https://wooliesx.herokuapp.com/api/answers/sort",
    "message": "High Sort Passed"
  },
  {
    "passed": true,
    "url": "https://wooliesx.herokuapp.com/api/answers/sort",
    "message": "Low Sort Passed"
  },
  {
    "passed": true,
    "url": "https://wooliesx.herokuapp.com/api/answers/sort",
    "message": "Recommended Sort Passed"
  }
]
```

# Exercise 3 - trolley calculator
[Trolley API](http://localhost:3000/api/answers/trolleyTotal)

## Result
```
[
  {
    "passed": true,
    "url": "https://powerful-retreat-80119.herokuapp.com/api/answers/trolleyTotal",
    "message": "Trolley total (0) returned correctly."
  },
  {
    "passed": true,
    "url": "https://powerful-retreat-80119.herokuapp.com/api/answers/trolleyTotal",
    "message": "Trolley total (14) returned correctly."
  }
]
```
