{
  Products: [ { Name: '1', Price: 2 }, { Name: '2', Price: 5 } ],
  Specials: [ {
    Quantities: [ { Name: '1', Quantity: 3 }, { Name: '2', Quantity: 0 } ],
    Total: 5
  }, {
    Quantities: [ { Name: '1', Quantity: 1 }, { Name: '2', Quantity: 2 } ],
    total: 10
  } ],
  Quantities: [ { Name: '1', Quantity: 3 }, { Name: '2', Quantity: 2 } ]
}
