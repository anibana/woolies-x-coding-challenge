const collectCoverageFrom = ['src/**/*.js']

const coverageDirectory = 'coverage'

const coverageReporters = ['text', 'lcov']

const moduleFileExtensions = ['js']

const testEnvironment = 'node'

const testRegex = '(/__tests__/.*|(\\.|/)(test|spec))\\.js$'

const moduleNameMapper = {
  '@core/(.*)': '<rootDir>/src/core/$1',
  '@config': '<rootDir>/src/config',
  '@shared/(.*)': '<rootDir>/src/v1/shared/$1',
}

module.exports = {
  collectCoverageFrom,
  coverageDirectory,
  coverageReporters,
  moduleFileExtensions,
  moduleNameMapper,
  testEnvironment,
  testRegex
}
