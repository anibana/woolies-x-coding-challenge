import Future from 'fluture'
import { INVALID_REQUEST } from '@core/errors'

export const withValidation = (api, validation) => (logger) => (req) => {
  try {
    validation.validateSync(req)

    return api(logger)(req)
  } catch (error) {
    return Future.reject({
      error: {
        code: INVALID_REQUEST,
        details: error.errors
      }
    })
  }
}
