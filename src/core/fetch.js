import rp from 'request-promise'
import Future, { tryP } from 'fluture'

const request = (fetch) => ({ body, params, headers, method, url, query }) => () =>
  fetch({ uri: url, qs: query, body, params, headers, method, json: true })

const createClient = (fetch) => (opts) => tryP(request(fetch)(opts))

const defaultClient = createClient(rp)

const fetchUsing = (method) => (opts) => (
  defaultClient({ ...opts, method })
    .chainRej((error) =>
      Future.reject({
        body: error.error || error.message,
        status: error.statusCode || 504,
        url: error.options.uri
      })
    )
)

export const Get = fetchUsing('GET')
export const Post = fetchUsing('POST')
export const Put = fetchUsing('PUT')
