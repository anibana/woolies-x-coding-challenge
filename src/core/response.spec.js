import { internalError, badRequest, ok } from './response'

describe('#ok', () => {
  it('responds with 200 status', () => {
    expect(ok('foo')).toEqual({ status: 200, body: 'foo' })
  })
})

describe('#internalError', () => {
  it('responds with 500 status', () => {
    expect(internalError('foo')).toEqual({ status: 500, body: 'foo' })
  })
})

describe('#badRequest', () => {
  it('badRequest with 400 status', () => {
    expect(badRequest('foo')).toEqual({ status: 400, body: 'foo' })
  })
})
