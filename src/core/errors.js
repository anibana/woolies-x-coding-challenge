import {
  badRequest,
  internalError
} from '@core/response'

export const INTERNAL_ERROR = 'INTERNAL_ERROR'
export const INVALID_REQUEST = 'INVALID_REQUEST'

const errorResponse = {
  [INTERNAL_ERROR]: internalError,
  [INVALID_REQUEST]: badRequest
}

export const getErrorInfo = ({ error }) => {
  try {
    const { code, details } = error
    const respond = errorResponse[code]

    if (!respond) {
      return internalError({
        code: INVALID_REQUEST,
        details: 'Internal server error'
      })
    }

    return respond({ code, details })
  } catch(e) {
    return internalError({
      code: INVALID_REQUEST,
      details: 'Internal server error'
    })
  }
}
