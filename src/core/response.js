export const ok = (body) => ({ status: 200, body })

export const badRequest = (body) => ({ status: 400, body })

export const internalError = (body) => ({ status: 500, body })
