import Future from 'fluture'
import winston, { format } from 'winston'
import 'winston-daily-rotate-file'
import path from 'path'
import { prettyPrint } from './pretty-print'

const { combine, json, timestamp } = format

const createTransports = () => {
  if (process.env.NODE_ENV !== 'local') {
    return [
      new winston.transports.DailyRotateFile({
        name: 'Woolies X',
        filename: path.join('logs', 'application-%DATE%.log'),
        datePattern: 'YYYY-MM-DD',
        json: true,
        zippedArchived: true
      })
    ]

  }

  return [new winston.transports.Console()]
}

const createFormat = () => {
  if (process.env.NODE_ENV !== 'local') {
    return combine(
      timestamp(),
      json()
    )
  }

  return combine(
    timestamp(),
    prettyPrint({ colorize: true })
  )
}

const logger = winston.createLogger({
  level: 'info',
  format: createFormat(),
  transports: createTransports()
})

const info = (correlationId) => (message) => (data) => {
  logger.info(message, { correlationId })

  return Future.of(data)
}

const logInfoJson = (correlationId) => (message, formatter = (a) => a) => (data) => {
  logger.info(message, { data: formatter(data), correlationId })

  return Future.of(data)
}

const error = (correlationId) => (message) => (data) => {
  logger.error(message, { correlationId })

  return Future.of(data)
}

const logJson = (correlationId) => (message, data) => {
  logger.info(message, { data, correlationId })
}

const logErrorJson = (correlationId) => (message, data) => {
  logger.error(message, { data, correlationId })
}

export default (correlationId) => ({
  info: info(correlationId),
  error: error(correlationId),
  logInfoJson: logInfoJson(correlationId),
  logJson: logJson(correlationId),
  logErrorJson: logErrorJson(correlationId)
})
