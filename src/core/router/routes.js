import express from 'express'
import uuid from 'uuid/v4'
import { differenceInMilliseconds } from 'date-fns'
import createLogger from '../logger'
import { getErrorInfo } from '../errors'

const createResponseFor = (api) => (request, res) => {
  const { path, headers, query, params } = request
  const correlationId = uuid()
  const logger = createLogger(correlationId)
  const startTime = new Date()

  return api(logger)(request)
    .promise()
    .then(({ status, body }) => {
      const duration = differenceInMilliseconds(new Date(), startTime)

      logger.logJson(`${path} response`, {
        duration,
        request: {
          body: request.body,
          headers,
          query,
          params
        },
        response: { status, body }
      })

      res.status(status)
      res.send(body)
    })
    .catch((error) => {
      console.log('error', error)
      const errorResponse = getErrorInfo(error)
      const duration = differenceInMilliseconds(new Date(), startTime)

      logger.logErrorJson(`${path} response`, {
        duration,
        request: {
          body: request.body,
          headers,
          query,
          params
        },
        response: { body: { error: errorResponse.body }, status: errorResponse.status }
      })

      res.status(errorResponse.status)
      res.send({ error: errorResponse.body })
    })
}

export default (routes) => (
  routes.reduce((router, { method, path, api }) => {
    return router[method](path, createResponseFor(api))
  }, express.Router())
)
