import { GET, POST, PUT } from './methods'

const path = '/path'
const api = 'api'

describe('#GET', () => {
  it('creates object with get method', () => {
    expect(GET(path)(api)).toEqual({
      method: 'get',
      path,
      api
    })
  })
})

describe('#POST', () => {
  it('creates object with post method', () => {
    expect(POST(path)(api)).toEqual({
      method: 'post',
      path,
      api
    })
  })
})

describe('#PUT', () => {
  it('creates object with put method', () => {
    expect(PUT(path)(api)).toEqual({
      method: 'put',
      path,
      api
    })
  })
})
