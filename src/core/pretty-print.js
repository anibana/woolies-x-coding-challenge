/* https://github.com/DABH/logform/blob/2fbf0cdd40a1de842a10e5e7702d4c9c119d19ae/pretty-print.js */

import { inspect } from 'util'
import { format } from 'logform'
import { LEVEL, MESSAGE, SPLAT } from 'triple-beam'

/*
 * function prettyPrint (info)
 * Returns a new instance of the prettyPrint Format that "prettyPrint"
 * serializes `info` objects. This was previously exposed as
 * { prettyPrint: true } to transports in `winston < 3.0.0`.
 */

export const prettyPrint = format((info, opts) => {
  const strippedInfo = Object.assign({}, info)
  delete strippedInfo[LEVEL]
  delete strippedInfo[SPLAT]
  info[MESSAGE] = inspect(strippedInfo, false, opts.depth || null, opts.colorize)
  return info
})
