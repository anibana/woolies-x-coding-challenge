import './config'
import './middleware'
import server from './server'

const loadApp = (middleware, port) => {
  server.load(middleware, port)
  server.on('listening', () => {
    console.log(`server started on port ${port}`)
  })
}

const loadDeps = ([{ default: middleware }, config]) => loadApp(middleware(), config.port)

Promise.all([
  import('./middleware'),
  import('./config')
])
  .then(loadDeps)
  .catch(console.log)

if (module.hot) {
  module.hot.accept()
}
