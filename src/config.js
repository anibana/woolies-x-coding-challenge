export const port = process.env.PORT || 3000
export const host = process.env.HOST || '0.0.0.0'
export const baseUrl = process.env.BASE_URL
export const token = process.env.TOKEN
