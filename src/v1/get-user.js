import Future from 'fluture'
import { ok } from '@core/response'

export default () => () => Future.of(ok({
  name: 'Aljon Aniban',
  token: '1234-455662-22233333-3333'
}))
