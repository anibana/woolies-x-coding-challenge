import { ok } from '@core/response'
import { withValidation } from '@core/validator'
import { getTrolleyCalculator } from '../services/get-trolley-calculator'
import { validation } from './validation'

const api = () => ({ body }) =>
  getTrolleyCalculator(body)
    .map(ok)

export default withValidation(api, validation)
