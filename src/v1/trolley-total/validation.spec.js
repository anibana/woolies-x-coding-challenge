import { validation } from './validation'

describe('#validation', () => {
  const request = {
    body: {
      products: [
        {
          name: 'Test Product A',
          price: 230
        }
      ],
      specials: [
        {
          quantities: [
            {
              name: 'Test Product A',
              quantity: 2
            }
          ],
          total: 2
        }
      ],
      quantities: [
        {
          name: 'Test Product A',
          quantity: 5
        }
      ]
    }
  }

  describe('when all values are provided correctly', () => {
    it('returns no errors', async () => {
      expect(() => {
        validation.validateSync(request)
      }).not.toThrow()
    })
  })

  it('returns error when the shape of the body is not correct', () => {
    const invalidRequest = {
      body: { foo: 'bar' }
    }

    expect(() => {
      validation.validateSync(invalidRequest)
    }).toThrow('body.quantities is a required field')
  })
})
