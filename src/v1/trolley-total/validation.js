import { array, object, string, number } from 'yup'

const product = object().shape({
  name: string(),
  price: number()
})

const quantity = object().shape({
  name: string(),
  quantity: number()
})

const special = object().shape({
  quantities: array().of(quantity),
  total: number()
})

export const validation = object().shape({
  body: object().shape({
    products: array().of(product).required(),
    specials: array().of(special).required(),
    quantities: array().of(quantity).required()
  })
})
