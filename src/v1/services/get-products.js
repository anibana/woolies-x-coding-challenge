import { Get } from '@core/fetch'
import { baseUrl, token } from '@config'

export const getProducts = () => (
  Get({ url: `${baseUrl}/products?token=${token}`})
)
