import { Get } from '@core/fetch'
import { baseUrl, token } from '@config'

export const getHistoryTally = (products) => (
  Get({ url: `${baseUrl}/shopperHistory?token=${token}`})
).map((history) => {
  const initialTally = products.reduce((tally, p) => ({ ...tally, [p.name]: 0 }), {})

  return history
    .flatMap(({ products }) => products)
    .reduce((tally, product) => ({
      ...tally,
      [product.name]: tally[product.name] + product.quantity
    }) , initialTally)
})
