import { Post } from '@core/fetch'
import { baseUrl, token } from '@config'

export const getTrolleyCalculator = (body) => (
  Post({ url: `${baseUrl}/trolleyCalculator?token=${token}`,  body })
).map(JSON.stringify)
