import Future from 'fluture'
import { Get } from '@core/fetch'
import { getHistoryTally } from './get-history-tally'

jest.mock('@core/fetch', () => ({
  Get: jest.fn()
}))

describe('#getHistoryTally', () => {
  const products = [{
    name: 'A',
    price: 1,
    quantity: 1
  }, {
    name: 'B',
    price: 2,
    quantity: 1
  }]

  const history = [{
    products: [{
      name: 'A',
      quantity: 1
    }, {
      name: 'B',
      quantity: 5
    }]
  }, {
    products: [{
      name: 'A',
      quantity: 1
    }, {
      name: 'B',
      quantity: 10
    }]
  }]

  it('calls shopperHistory API and creates a mapping for product name and total count', () => {
    Get.mockImplementationOnce(jest.fn(() => Future.of(history)))

    expect(getHistoryTally(products).promise()).resolves.toEqual({ A: 2, B: 15 })
  })
})
