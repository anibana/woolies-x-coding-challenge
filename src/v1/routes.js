import routes from '@core/router/routes'
import { GET, POST } from '@core/router/methods'
import getUserApi from './get-user'
import sortApi from './sort'
import trolleyTotalApi from './trolley-total'

export default routes([
  GET('/api/answers/user')(getUserApi),
  GET('/api/answers/sort')(sortApi),
  POST('/api/answers/trolleyTotal')(trolleyTotalApi)
])
