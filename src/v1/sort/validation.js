import { object, string } from 'yup'
import { SORT_OPTIONS } from './options'

export const validation = object().shape({
  query: object().shape({
    sortOption: string().oneOf(SORT_OPTIONS)
  })
})
