export const LOW = 'Low'
export const HIGH = 'High'
export const ASCENDING = 'Ascending'
export const DESCENDING = 'Descending'
export const RECOMMENDED = 'Recommended'

export const SORT_OPTIONS = [LOW, HIGH, ASCENDING, DESCENDING, RECOMMENDED]
