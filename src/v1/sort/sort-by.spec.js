import { ASCENDING, DESCENDING, LOW, HIGH } from './options'
import { sortBy } from './sort-by'

describe('#sortBy', () => {
  const products = [{
    name: 'A',
    price: 99,
    quantity: 0
  }, {
    name: 'B',
    price: 1,
    quantity: 0
  }]

  it('returns products in ascending order by name if sortOption is "Ascending"', () => {
    expect(sortBy(ASCENDING)(products)).toEqual([{
      name: 'A',
      price: 99,
      quantity: 0
    }, {
      name: 'B',
      price: 1,
      quantity: 0
    }])
  })

  it('returns products in descending order by name if sortOption is "Descending"', () => {
    expect(sortBy(DESCENDING)(products)).toEqual([{
      name: 'B',
      price: 1,
      quantity: 0
    }, {
      name: 'A',
      price: 99,
      quantity: 0
    }])
  })

  it('returns products in ascending order by price if sortOption is "LOW"', () => {
    expect(sortBy(LOW)(products)).toEqual([{
      name: 'B',
      price: 1,
      quantity: 0
    }, {
      name: 'A',
      price: 99,
      quantity: 0
    }])
  })

  it('returns products in descending order by price if sortOption is "HIGH"', () => {
    expect(sortBy(HIGH)(products)).toEqual([{
      name: 'A',
      price: 99,
      quantity: 0
    }, {
      name: 'B',
      price: 1,
      quantity: 0
    }])
  })

})
