import {
  ASCENDING,
  DESCENDING,
  LOW,
  HIGH
} from './options'

const SORT_BY = {
  [ASCENDING]: (a, b) => (a.name < b.name ? -1 : 1),
  [DESCENDING]: (a, b) => (a.name > b.name ? -1 : 1),
  [LOW]: (a, b) => (a.price - b.price),
  [HIGH]: (a, b) => (b.price - a.price)
}

export const sortBy = (sortOption) => (products) => products.sort(SORT_BY[sortOption])
