import Future from 'fluture'
import { ok } from '@core/response'
import { withValidation } from '@core/validator'
import { sortBy } from './sort-by'
import { sortByPopularity } from './sort-by-popularity'
import { RECOMMENDED } from './options'
import { getProducts } from '../services/get-products'
import { validation } from './validation'

const api = () => ({ query: { sortOption } }) => (
  getProducts()
    .chain((products) => {
      if (sortOption === RECOMMENDED) {
        return sortByPopularity(products)
      }

      return Future.of(sortBy(sortOption)(products))
    })
    .map(ok)
)

export default withValidation(api, validation)
