import Future from 'fluture'
import { getHistoryTally } from '../services/get-history-tally'

export const sortByPopularity = (products) => Future.of(products)
  .chain(getHistoryTally)
  .map((tally) => products.sort((a, b) => tally[b.name] - tally[a.name]))
