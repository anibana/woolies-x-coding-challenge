import Future from 'fluture'
import { Get } from '@core/fetch'
import { sortByPopularity } from './sort-by-popularity'

jest.mock('@core/fetch', () => ({
  Get: jest.fn()
}))

describe('#sortByPopularity', () => {
  const products = [{
    name: 'A',
    price: 1,
    quantity: 1
  }, {
    name: 'B',
    price: 2,
    quantity: 1
  }]

  const history = [{
    products: [{
      name: 'A',
      quantity: 1
    }, {
      name: 'B',
      quantity: 5
    }]
  }, {
    products: [{
      name: 'A',
      quantity: 1
    }, {
      name: 'B',
      quantity: 10
    }]
  }]

  it('fetches shopperHistory API and sort products by popularity', () => {
    Get.mockImplementationOnce(jest.fn(() => Future.of(history)))

    expect(sortByPopularity(products).promise()).resolves.toEqual([{
      name: 'B',
      price: 2,
      quantity: 1
    }, {
      name: 'A',
      price: 1,
      quantity: 1
    }])
  })
})
