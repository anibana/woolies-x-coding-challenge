import { validation } from './validation'

describe('#validation', () => {
  const request = {
    query: { sortOption: 'Low' }
  }

  describe('when all values are provided correctly', () => {
    it('returns no errors', async () => {
      expect(() => {
        validation.validateSync(request)
      }).not.toThrow()
    })
  })

  it('returns error when sortOption is not in the allowed list of values', () => {
    const invalidRequest = {
      query: { sortOption: 'FOO' }
    }

    expect(() => {
      validation.validateSync(invalidRequest)
    }).toThrow('query.sortOption must be one of the following values: Low, High, Ascending, Descending, Recommended')
  })
})
