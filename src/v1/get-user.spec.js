import getUser from './get-user'

describe('#getUser', () => {
  it('returns name and token', () => {
    expect(getUser()().promise()).resolves.toEqual({
      status: 200,
      body: {
        name: 'Aljon Aniban',
        token: '1234-455662-22233333-3333'
      }
    })
  })
})
