import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import bodyParser from 'body-parser'

import v1Apis from './v1/routes'

export default () => {
  const app = express()

  app.use(cors())

  if (process.env.NODE_ENV !== 'test') {
    app.use(morgan('dev'))
  }

  app.use(bodyParser.json())

  app.use(v1Apis)

  return app
}
